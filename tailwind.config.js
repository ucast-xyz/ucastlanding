/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{jsx,js}"],
  theme: {
    extend: {
      colors: {
        primary: {
          100: "#FED035",
          50: "rgba(254, 208, 53, 0.15)",
        },
        green: {
          990: "#30A280",
          50: "rgba(48, 162, 128, 0.26)"
        },
        red: {
          990: "#F00073",
          50: "rgba(240, 0, 115, 0.26)"
        },
        pink: {
          990: "#E459B5",
          50: "rgba(228, 89, 181, 0.26)"
        },
        purple: {
          990: "#985AD3",
          50: "rgba(152, 90, 211, 0.26)"
        },
        yellow: {
          990: "rgba(246, 152, 42, 0.8)",
          50: "rgba(246, 152, 42, 0.26)"
        },
        blue: {
          990: "#395CF9",
          50: "rgba(57, 92, 249, 0.26)"
        },
        grey: {
          990: "#B9B9B9",
        },
      },
    },
  },
  plugins: [],
};
