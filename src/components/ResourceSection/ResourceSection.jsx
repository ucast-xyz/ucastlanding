import { useContext } from "react";
import { mobileDeviceContext } from "../../App";
import PodImg from "../../assets/ucast_podcast_res.png";
import analyticsProvider from "../../utils/analyticsProvider";
import "./ResourceSection.scss";

const ResourceSection = () => {
  const isMobile = useContext(mobileDeviceContext);

  const onSignupCick = () => {
    analyticsProvider.sendEvent("website::click_signup_button");
    window.location.href = "https://app.ucast.xyz/podcaster";
  };

  return (
    <div className="resources">
      <div className="content">
        <div className="img">
          <img src={PodImg} alt="Podcast" />
          {!isMobile && (
            <button onClick={onSignupCick} className="ucast-light-btn">
              Join Now
            </button>
          )}
        </div>

        <div className="text">
          <p className="types">TYPES OF CAMPANIGN </p>
          <h1>
            Everything you need <br /> in one place
          </h1>

          <div className="resourceInfo">
            <h6>Resources</h6>
            <p>
              Scroll through our curated list of hundreds of resources and claim
              offers that will help you grow your podcast.
            </p>
          </div>

          {isMobile && (
            <div className="text-center">
              <button onClick={onSignupCick} className="ucast-light-btn">
                Join Now
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ResourceSection;
