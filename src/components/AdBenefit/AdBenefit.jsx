import './AdBenefit.scss'

const AdBenefit = ({title, content, icon}) => {
    return (
      <div className="adBenefit">
        <div className="adBenefit__icon">
          {icon}
        </div>

        <div className="adBenefit__content">
          <h6>{title}</h6>
          <p>
            {content}
          </p>
        </div>
      </div>
    );
}

export default AdBenefit